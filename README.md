# Проект: "Создание дизайна страниц для фирмы Фокуссмайл"

## Задачи
- Изучение целевой аудитории для создания клиентоориентированного дизайна
- Изучение функционала графического онлайн-редактора Фигма
- Обновление дизайна главной страницы и карточки товара

# Участники

| Учебная группа | Telegram           | ФИО                      |
|----------------|--------------------|--------------------------|
| 211-321        | @kreesh            | Крошкин Е.М.             |
| 211-321        | @b4r4n0vs          | Баранов А.А.             |
| 191-321        | @svichnikValentina |Свичник В.А.              |

# Личный вклад участников

## Крошкин Егор 
- Изучение возможностей редактора Photoshop и работа в нем (4ч)
- Прохождение курса по работе в онлайн-редакторе Фигма (6ч)
- Определение целевой аудитории и общих концептов дизайна сайта (4ч)
- Общение с куратором и заказчиком проекта (6ч)
- Создание дизайна (12ч)
- Подготовка отчета (4ч)
- Анализ конкурентов (4ч)
- Проектирование сайта (6ч)
- Понятие сути проекта, постановки цели, разработка плана (8ч)
- Исправление замечаний заказчика (4ч)
- Общий объем: 58ч
Свой вариант дизайна: https://www.figma.com/file/somqWDAvwFTQ2OxLpCqRAR/Untitled?node-id=0%3A1


## Баранов Антон
- Изучение возможностей редактора Photoshop и работа в нем (4ч)
- Прохождение курса по работе в онлайн-редакторе Фигма (8ч)
- Определение целевой аудитории и общих концептов дизайна сайта (4ч)
- Общение с куратором проекта (5ч)
- Анализ конкурентов (4ч)
- Проектирование сайта (1ч)
- Создание дизайна (12ч)
- Подготовка отчета (6ч)
- Исправление замечаний заказчика (4ч)
- Объединение трех дизайнов один (1ч)
- Общий объем: 49ч

Свой вариант дизайна: https://www.figma.com/file/sQLCaNdVe0SxdeaNpyyz2e/%D0%A4%D0%BE%D0%BA%D1%83%D1%81%D1%81%D0%BC%D0%B0%D0%B9%D0%BB2.0?node-id=0%3A1

## Свичник Валентина
- Анализ конкурентов (2ч)
- Анализ компании заказчика (1ч)
- Анализ сайта заказчика (2ч)
- Определение цветового решения (1ч)
- Подбор шрифтов (1ч)
- Ребрендинг логотипа (2ч)
- Создание навигационного меню (1ч)
- Создание первого экрана сайта (3ч)
- Разработка макета главной страницы (6ч)
- Разработка второго варианта главной страницы (1ч)
- Упорядочивание стилей для шрифтов в выбранном макете (1ч)
- Внесение корректировок по расположению элементов относительно сетки grid (1ч)
- Доработка главной страницы сайта (5ч)
- Создание макета страницы «портрет» (6ч)
- Участие в онлайн конференциях с заказчиком (4ч)
- Онлайн общение с заказчиком и корректировка макета в соответствии с правками (2ч)
- Общий объем: 39ч

Свой вариант дизайна: https://www.figma.com/file/uVLl4jrXThWVScVPJphKzo/Untitled?node-id=31%3A4

# Результат

https://www.figma.com/file/3L5LiHlLmewkNoKIso2qNt/My-magic-painting?node-id=0%3A1
